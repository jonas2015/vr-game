﻿using UnityEngine;
using System.Collections;

public class Gliding : MonoBehaviour
{
    [Tooltip("The gravity force applied to this game object")]
    public float balanceSpeed;
    [Tooltip("This is the initial force applied to the gliding character")]
    public float glideSpeed = 100f;
    [Tooltip("The turning limit of the character on the horizontal axis")]
    public float turningLimit = 50f;
    [Tooltip("The velocity this object fall down")]
    public float stallGravity = 25f;
    [Tooltip("Character velocity limit, the character can't go past this speed value")]
    public float velocityLimit = 300f;
    public float minimumForwardSpeed = 20f;
    

    /// <summary>
    /// The camera rotation at the start
    /// </summary>
    private Quaternion initialCameraRotation;
    private Quaternion initialCameraLocalRotation;
    private Rigidbody _rigidbody;

    //rotation tracking
    private float currentRotationX;
    private float currentRotationY;
    private float currentRotationZ;
    private float initialGlideSpeed;
    private float initialVelocityLimit;
    private bool landed;

    private void Awake()
    {
        Cache();
    }

    private void Update()
    {

    }

    private void FixedUpdate()
    {
        if(!landed)
        {
            RotateCharacter();
            Glide();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        Crush();
    }

    private void Crush()
    {
        //set landed boolean
        landed = true;
        //set the velocity limit to 10 to instantly slow down and simulate an impact
        velocityLimit = 10;
        //increase drag to slow down the rigidbody faster
        _rigidbody.drag = 1;
    }

    private void Glide()
    {
        //create forward velocity
        Vector3 glideVelocity = transform.forward * glideSpeed;
        Vector3 gravityForce = new Vector3(0, Physics.gravity.y, 0);
        glideVelocity += gravityForce;

        //if we are going up
        if(transform.rotation.eulerAngles.x >= 180 && transform.rotation.eulerAngles.x <= 360)
        {
            //and character stil has momentum, decrease untill it's 0
            if(glideSpeed > minimumForwardSpeed)
                glideSpeed -= velocityLimit/600;
        }
        // if we are going down
        else if(transform.rotation.eulerAngles.x < 180)
        {
            //increase speed
            glideSpeed += velocityLimit/500;
        }
        //and if we are perfectly straight 
        else
        {
            //if the current speed we have from going bottom is higher the the initial speed
            //we slowly decrease it until it return to it's original value
            //if (glideSpeed != initialGlideSpeed)
            glideSpeed = Mathf.MoveTowards(glideSpeed, initialGlideSpeed, 15 * Time.deltaTime);
        }

        if(glideSpeed <= minimumForwardSpeed)
        {
            // _rigidbody.AddForce(Vector3.down * stallGravity, ForceMode.Acceleration);
            glideVelocity += gravityForce * stallGravity;
        }
        //clamp max glide speed
        glideSpeed = Mathf.Clamp(glideSpeed, 0f, velocityLimit);

        //set velocity 
        _rigidbody.velocity = glideVelocity;

        _rigidbody.velocity = Vector3.ClampMagnitude(_rigidbody.velocity, velocityLimit);
    }

    private void RotateCharacter()
    {
        //store input value from controller
        float horizontalAxis = Input.GetAxis("Horizontal");
        float verticalAxis = Input.GetAxis("Vertical");

        float tip = (transform.right + Vector3.up).magnitude - 1.414214f;
        currentRotationY -= tip;



        //if(currentRotationZ != 0 && horizontalAxis == 0)
        //currentRotationZ = Mathf.MoveTowards(currentRotationZ, 0, Time.deltaTime * balanceSpeed);

        if(horizontalAxis != 0)
        {
            currentRotationZ -= horizontalAxis*1.2f;
            //transform.Rotate(transform.forward, horizontalAxis * Time.deltaTime * -75, Space.World);
        }

        //limit rotation on X and Z axis
        //currentRotationX = Mathf.Clamp(currentRotationX, -50, 50);
        //currentRotationZ = Mathf.Clamp(currentRotationZ, -turningLimit, turningLimit);

        //create the final character rotation        
        Vector3 characterRotation = new Vector3(currentRotationX, currentRotationY, currentRotationZ);

        //set the final rotation 
         transform.eulerAngles = characterRotation;

        //increase rotation values with inputs
        if(verticalAxis != 0)
        {
            //currentRotationX = verticalAxis + (Mathf.Cos(currentRotationZ)*currentRotationX);
            transform.Rotate(transform.right, verticalAxis * Time.deltaTime * 50, Space.World);
        }
        else if(verticalAxis == 0 && currentRotationX != 0)
        {
            //currentRotationX = Mathf.MoveTowards(currentRotationX, 0, Time.deltaTime * balanceSpeed);
        }

        currentRotationX = transform.rotation.eulerAngles.x;
        currentRotationY = transform.rotation.eulerAngles.y;
        currentRotationZ = transform.rotation.eulerAngles.z;
    }

    private void Cache()
    {
        _rigidbody = GetComponent<Rigidbody>();

        currentRotationX = transform.rotation.eulerAngles.x;
        currentRotationY = transform.rotation.eulerAngles.y;
        currentRotationZ = transform.rotation.eulerAngles.z;

        initialGlideSpeed = glideSpeed;
        initialVelocityLimit = velocityLimit;
    }

    public void reset()
    {
        glideSpeed = initialGlideSpeed;
        velocityLimit = initialVelocityLimit;
        currentRotationX = transform.rotation.eulerAngles.x;
        currentRotationY = transform.rotation.eulerAngles.y;
        currentRotationZ = transform.rotation.eulerAngles.z;
        _rigidbody.drag = 0;
        landed = false;
    }
}
