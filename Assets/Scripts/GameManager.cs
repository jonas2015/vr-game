﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public GameObject pauseMenu = null;
    public GameObject player = null;
    public GameObject[] spawnPoints = null;
    public Text scoreText = null;
    //public GameObject spawnedPlayer = null;
    int score = 0, scoreTarget = 0;
    float countInterval = 0.05f, timer = 0;

	// Use this for initialization
	void Start ()
    {
        respawn();
        pauseUnpauseGame();
	}
	
	// Update is called once per frame
	void Update ()
    {
        writeScore();

        if(Input.GetButtonDown("Cancel"))
        {
            pauseUnpauseGame();
        }
	}

    void FixedUpdate()
    {
        timer += Time.deltaTime;

        if(score < scoreTarget && timer >= countInterval)
        {
            score++;
            timer = 0;
        }
    }

    public void pauseUnpauseGame()
    {
        if(pauseMenu == null)
            return;
        if(pauseMenu.active)
        {
            pauseMenu.SetActive(false);
            Time.timeScale = 1;
        }
        else
        {
            pauseMenu.SetActive(true);
            Time.timeScale = 0;
        }
    }

    public void restartGame()
    {
        scoreTarget = 0;
        score = 0;
        pauseUnpauseGame();
        respawn();
        //Application.LoadLevel(Application.loadedLevel);
    }

    public void closeGame()
    {
        Application.Quit();
    }

    public void increaseScore(int plus)
    {
        scoreTarget += plus;
    }

    void writeScore()
    {
        scoreText.text = score.ToString();
    }

    void respawn()
    {
        if(spawnPoints != null && player != null)
        {
            //Destroy(spawnedPlayer);
            //spawnedPlayer = (GameObject) Instantiate(player, spawnPoints[0].transform.position, spawnPoints[0].transform.rotation);
            player.transform.position = spawnPoints[0].transform.position;
            player.transform.rotation = spawnPoints[0].transform.rotation;
            player.GetComponent<Gliding>().reset();
        }
    }
}
