﻿using UnityEngine;
using System.Collections;

public class onTorusCollision : MonoBehaviour {

    public int givenScore = 1;
    GameManager managerScript = null;
    float scoreTimer = 0f;

	// Use this for initialization
	void Start () {
        managerScript = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
	}

    void FixedUpdate()
    {
        if(scoreTimer > 0)
            scoreTimer -= Time.deltaTime;
    }

    void OnTriggerEnter(Collider col)
    {
        if(scoreTimer <= 0)
        {
            managerScript.increaseScore(givenScore);
            scoreTimer = 1;
        }
    }
}
