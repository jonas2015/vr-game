﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonSelected : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField]
    MenuNavigation menuNavigation = null;

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("onpointer");
        Button but = gameObject.GetComponent<Button>();
        if(menuNavigation != null)
        {
            menuNavigation.mouseSelected = true;
            menuNavigation.mouseSelectedButton(but);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if(menuNavigation != null)
            menuNavigation.mouseSelected = false;
    }
}