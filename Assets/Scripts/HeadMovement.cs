﻿using UnityEngine;
using System.Collections;

public class HeadMovement : MonoBehaviour
{
    [SerializeField]
    float verticalSensitivity = 1f;
    [SerializeField]
    float horizontalSensitivity = 1f;

    Vector3 v3Rotation = Vector3.zero;

    Quaternion cameraRotation;

    // Use this for initialization
    void Start()
    {
        cameraRotation = transform.localRotation;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.timeScale != 0)
        {
            v3Rotation.x -= verticalSensitivity * Input.GetAxis("Mouse Y");
            v3Rotation.y += horizontalSensitivity * Input.GetAxis("Mouse X");

            //v3Rotation.x = Mathf.Clamp(v3Rotation.x, -90, 90);
            //v3Rotation.y = Mathf.Clamp(v3Rotation.y, -90, 90);
            transform.localEulerAngles = v3Rotation;
        }
    }
}
