﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MenuNavigation : MonoBehaviour
{
    [SerializeField]
    Button[] buttons = null;
    public bool mouseSelected = false;
    int selectedButton = 0;
    int buttonsAmount = 0;
    bool buttonHeld = false;

	// Use this for initialization
	void Start ()
    {
        buttonsAmount = buttons.Length-1;
        if(buttons != null && buttons.Length > 0)
            buttons[0].Select();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Debug.Log(mouseSelected);
        if(Input.GetAxisRaw("JoystickVertical") != 0)
        {
            if(!buttonHeld)
            {
                if(Input.GetAxis("JoystickVertical") > 0 && !mouseSelected)
                    selectUp();
                else if(Input.GetAxis("JoystickVertical") < 0 && !mouseSelected)
                    selectDown();
                buttonHeld = true;
            }
        }
        else if(Input.GetAxisRaw("JoystickVertical") == 0)
            buttonHeld = false;
	}

    void selectDown()
    {
        selectedButton++;
        if(selectedButton > buttonsAmount)
            selectedButton = 0;
        buttons[selectedButton].Select();
    }

    void selectUp()
    {
        selectedButton--;
        if(selectedButton < 0)
            selectedButton = buttonsAmount;
        buttons[selectedButton].Select();
    }

    public void mouseSelectedButton(Button selectedBut)
    {
        for(int i = 0; i < buttons.Length; i++)
        {
            if(buttons[i] == selectedBut)
            {
                
                selectedButton = i;
                buttons[selectedButton].Select();
            }
        }
    }
}
