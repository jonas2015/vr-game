﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour
{
    [SerializeField]
    float airspeed = 1f;
    [SerializeField]
    float xVelocity = 1f;
    [SerializeField]
    Vector3 angle;
    [SerializeField]
    float fall = 1f;
    [SerializeField]
    Transform tiltometer = null;
    [SerializeField]
    float initialVelocity = 1f;
    [SerializeField]
    float forwardVelocityIncreaseW = 1f;
    [SerializeField]
    float forwardVelocityIncreaseS = 1f;
    [SerializeField]
    float upVelocityIncreaseS = 1f;
    [SerializeField]
    ForceMode forwardModeW = ForceMode.Acceleration;
    [SerializeField]
    ForceMode forwardModeS = ForceMode.Acceleration;
    [SerializeField]
    ForceMode upModeS = ForceMode.Acceleration;
    float startingVelocity = 25f;

    Rigidbody rig = null;

	// Use this for initialization
	void Start ()
    {
        rig = GetComponent<Rigidbody>();
        //rig.AddForce(transform.forward * initialVelocity, ForceMode.VelocityChange);
        rig.velocity = transform.forward * startingVelocity;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        
        float roll = Input.GetAxis("Horizontal");
        float tilt = Input.GetAxis("Vertical");

        float yaw = Input.GetAxis("Yaw") / 8;

        roll /= Time.timeScale;
        tilt /= Time.timeScale;
        yaw /= Time.timeScale;

        float tip = (transform.right + Vector3.up).magnitude - 1.414214f;
        yaw -= tip;

        if((transform.forward + rig.velocity.normalized).magnitude < 1.4)
            tilt += 0.3f;

        if(tilt != 0)
            transform.Rotate(transform.right, tilt * Time.deltaTime * 20, Space.World);
        if(roll != 0)
            transform.Rotate(transform.forward, roll * Time.deltaTime * -20, Space.World);
        if(yaw != 0)
            transform.Rotate(Vector3.up, yaw * Time.deltaTime * 15, Space.World);

        if(Input.GetButton("Jump"))
        {
            rig.AddForce(transform.forward * Time.deltaTime * 1000);
        }

        rig.velocity -= Vector3.up * Time.deltaTime;

        Vector3 vertvel = rig.velocity - Vector3.Exclude(transform.up, rig.velocity);
        fall = vertvel.magnitude;
        rig.velocity -= vertvel * Time.deltaTime;
        rig.velocity += vertvel.magnitude * transform.forward * Time.deltaTime / 10;

        Vector3 forwardDrag = rig.velocity - Vector3.Exclude(transform.forward, rig.velocity);
        rig.AddForce(-forwardDrag * forwardDrag.magnitude * Time.deltaTime / 1000);

        Vector3 sideDrag = rig.velocity - Vector3.Exclude(transform.right, rig.velocity);
        rig.AddForce(-sideDrag * sideDrag.magnitude * Time.deltaTime);
        
        airspeed = rig.velocity.magnitude;
        xVelocity = transform.InverseTransformDirection(rig.velocity).z;
        tiltometer.rotation = Quaternion.LookRotation(Vector3.up);
        //rig.AddForce(0f, 0f, 5f);
        //transform.forward = rig.velocity.normalized;
        /*Quaternion addRot = Quaternion.identity;
        if(Input.GetKey(KeyCode.W))
        {
            
            Debug.Log("Pressing W");
            addRot.eulerAngles = new Vector3(0.4f, 0, 0);
            rig.rotation *= addRot;
            //rig.AddForce(transform.forward * forwardVelocityIncreaseW, forwardModeW);
        }
        else if(Input.GetKey(KeyCode.S))
        {
            Debug.Log("Pressing S");
            addRot.eulerAngles = new Vector3(-0.4f, 0, 0);
            rig.rotation *= addRot;
            //rig.AddForce(transform.forward * forwardVelocityIncreaseS, forwardModeS);
            //rig.AddForce(transform.up * upVelocityIncreaseS, upModeS);
        }
        //rig.AddForce((-Vector3.up * 90.8f));
        //rig.AddForce(Vector3.forward * airspeed);
        
        angle = transform.forward;
        startingVelocity = startingVelocity * (1 - angle.y);
        if(startingVelocity > 50f)
            startingVelocity = 50f;
        rig.velocity = transform.forward * startingVelocity;*/
        //transform.position += transform.forward * Time.deltaTime * 10 * -angle.y;
        /*if(charController.isGrounded)
        {
            Debug.Log("grounded");

            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= moveSpeed;
            if(Input.GetAxis("Horizontal") != 0 && Input.GetAxis("Horizontal") != 0)
                moveDirection *= 0.7071f;

            currentFallVelocity = 0f;

            if(Input.GetButton("Jump"))
                currentFallVelocity = jumpSpeed;
        }
        else if(!charController.isGrounded)
        {
            currentFallVelocity = -gravity * Time.deltaTime;
            //moveDirection *= moveSpeed / 5;
        }

        moveDirection.y += currentFallVelocity;
        charController.Move(moveDirection * Time.deltaTime);*/
	}
}
