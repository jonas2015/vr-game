**ZAIDZIAMA VERSIJA**
Repositorijoje pavadinta "WingsuitGlider application.zip"

---------

**Projekto tema:**

Virtual reality zaidimas naudojant �Oculus Rift� akinius sukurti virtualios realyb�s iliuzija. Zaidimo tematika: skraidymas su wingsuit kostiumu po kalnuota aplink�.

**Komandos sudetis (Pavarde, Vardas, grupe, elektroninis pastas)**

1. Ausevicius Jonas, IF-4/15, jonas.ausevicius@ktu.edu

**Naudojami projekto kurimo irankiai (programavimo kalba, IDE, karkasai, ...)**

�	Unity zaidim� variklis

�	C# programavimo kalba

�	BitBucket ir SourceTree (versiju kontrolei)

�	EasyBacklog

�	Visual Studio IDE

**Trumpas funkcinis programos aprasymas (5-6 sakiniai)**

zaidimas programuojamas C# programavimo kalba, naudojama Visual Studio aplinka. Naudojamas Unity zaidimu variklis kartu su Oculus Rift SDK virtualiai realybei sukurti. Zaid�jas gales galvos judesiu apzi�reti pacia aplinka. Valdymui naudojama klaviatura ir pele arba joystikas. Programos tikslas suteikti vartotojui jausma, lyg jis pats skraido su wingsuit kostiumu, bet kartu ir sumazinti nepatogu motion sickness jausma. 

**Bitbucket nuoroda i projekta:**

�	https://bitbucket.org/jonas2015/vr-game

**Nurodykite pasirinkta susitikimo su destytojais laika:**

�	Treciadienis, 10:30 - 12:00, lygines savaites